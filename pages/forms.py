from phonenumber_field.formfields import PhoneNumberField
import datetime

from django import forms
from django.core.mail import send_mail
from django.conf import settings

class ContactForm(forms.Form):
    name = forms.CharField()
    email = forms.EmailField()
    phone_number = PhoneNumberField(label="Phone Number (+63 xxx xxx xxxx)")
    rental_start_date = forms.DateField(
        initial=datetime.date.today,
        label="Rental Start Date (yyyy-mm-dd)"
        )
    rental_end_date = forms.DateField(
        initial=datetime.date.today,
        label="Rental End Date (yyyy-mm-dd)",
        )
    details = forms.CharField(widget=forms.Textarea(attrs={
        'rows': 5,
    }))

    error_css_class = "error"

    #! TEMPORARY
    def send_email(self, context):
        email_from = settings.EMAIL_HOST_USER
        recepient_list = settings.RECIPIENT_LIST
        
        name = context['name']
        email = context['email']
        phone_number = context['phone_number']
        rental_start_date = context['rental_start_date']
        rental_end_date = context['rental_end_date']
        details = context['details']

        email_subject = f"{name} from {rental_start_date} to {rental_end_date}"
        email_content = f"Name: {name}\nEmail: {email}\nPhone Number: {phone_number}\nRental Start Date: {rental_start_date}\nRental End Date: {rental_end_date}\n\n{details}"
        print(email_content)
        send_mail(
            email_subject, 
            email_content, 
            email_from, 
            recepient_list,
            fail_silently=False
        )
