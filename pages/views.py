from django.conf import settings
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormView

from .forms import ContactForm

# Create your views here.

class BaseView(FormView):
    form_class = ContactForm
    success_url = reverse_lazy('email_success')
    fail_url = reverse_lazy('email_fail')

    def form_valid(self, form):
        context = form.cleaned_data
        try:
          form.send_email(context)
          return super().form_valid(form)
        except Exception as e:
          print(e)
          return HttpResponseRedirect(self.fail_url)
          


class HomeView(BaseView):
    template_name = 'home.html' 


class AboutView(BaseView):
    template_name = 'about.html'


class CarView(BaseView):
    template_name = 'cars.html'


class ContactMobileView(BaseView):
    template_name = 'contact_mobile.html'


class ContactView(BaseView):
    template_name = 'contact.html'


# SPECIFIC CAR PAGES

class SedanViosView(BaseView):
    template_name = 'vehicle_sedan_vios.html'


class SedanAltisView(BaseView):
    template_name = 'vehicle_sedan_altis.html'


class SedanCityView(BaseView):
    template_name = 'vehicle_sedan_city.html'


class SuvFortunerView(BaseView):
    template_name = 'vehicle_suv_fortuner.html'


class SuvInnovaView(BaseView):
    template_name = 'vehicle_suv_innova.html'


class SuvRav4View(BaseView):
    template_name = 'vehicle_suv_rav4.html'


class VanGrandiaView(BaseView):
    template_name = 'vehicle_van_grandia.html'


class VanSuperGrandiaView(BaseView):
    template_name = 'vehicle_van_supergrandia.html'


class VanNV350View(BaseView):
    template_name = 'vehicle_van_nv350.html'


class LuxuryAlphardView(BaseView):
    template_name = 'vehicle_luxury_alphard.html'


class LuxuryBenzView(BaseView):
    template_name = 'vehicle_luxury_benz.html'


class LuxuryH350View(BaseView):
    template_name = 'vehicle_luxury_h350.html'


class DeliveryL300View(BaseView):
    template_name = 'vehicle_delivery_l300.html'


class DeliveryHiluxView(BaseView):
    template_name = 'vehicle_delivery_hilux.html'


# INFO PAGES

class EmailSuccessView(TemplateView):
    template_name = 'email_success.html'

class EmailFailView(TemplateView):
    template_name = 'email_fail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['email'] = settings.EMAIL_HOST_USER
        return context


class TermsAndConditionsView(BaseView):
    template_name = 'terms_and_conditions.html'


class ImageCreditView(BaseView):
    template_name = 'image_credit.html'
