from django.urls import path

from .views import (
    HomeView,
    AboutView,
    CarView,
    ContactView,
    ContactMobileView,
)

# specific cars
from .views import SedanViosView, SedanAltisView, SedanCityView, SuvFortunerView, SuvInnovaView, SuvRav4View, VanGrandiaView, VanSuperGrandiaView, VanNV350View, LuxuryAlphardView, LuxuryBenzView, LuxuryH350View, DeliveryL300View, DeliveryHiluxView

# template pages
from .views import EmailSuccessView, TermsAndConditionsView, ImageCreditView, EmailFailView

urlpatterns = [
    path('about/', AboutView.as_view(), name='about'),
    path('cars/', CarView.as_view(), name='cars'),
    path('cars/vios/', SedanViosView.as_view(), name='cars_vios'),
    path('cars/altis/', SedanAltisView.as_view(), name='cars_altis'),
    path('cars/city/', SedanCityView.as_view(), name='cars_city'),
    path('cars/fortuner/', SuvFortunerView.as_view(), name='cars_fortuner'),
    path('cars/innova/', SuvInnovaView.as_view(), name='cars_innova'),
    path('cars/rav4/', SuvRav4View.as_view(), name='cars_rav4'),
    path('cars/grandia/', VanGrandiaView.as_view(), name='cars_grandia'),
    path('cars/supergrandia/', VanSuperGrandiaView.as_view(), name='cars_supergrandia'),
    path('cars/nv350/', VanNV350View.as_view(), name='cars_nv350'),
    path('cars/alphard/', LuxuryAlphardView.as_view(), name='cars_alphard'),
    path('cars/benz/', LuxuryBenzView.as_view(), name='cars_benz'),
    path('cars/h350/', LuxuryH350View.as_view(), name='cars_h350'),
    path('cars/l300/', DeliveryL300View.as_view(), name='cars_l300'),
    path('cars/hilux/', DeliveryHiluxView.as_view(), name='cars_hilux'),
    path('contact/', ContactView.as_view(), name='contact'),
    path('contact/mobile/', ContactMobileView.as_view(), name='contact_mobile'),
    path('email_success/', EmailSuccessView.as_view(), name='email_success'),
    path('email_fail/', EmailFailView.as_view(), name='email_fail'),
    path('terms_and_conditions/', TermsAndConditionsView.as_view(), name='terms_and_conditions'),
    path('image_credit/', ImageCreditView.as_view(), name='image_credit'),
    path('', HomeView.as_view(), name='home'),
]
