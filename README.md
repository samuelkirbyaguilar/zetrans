# Zetrans

Environment variables:

```
SECRET_KEY
EMAIL_USER
EMAIL_PASS
EMAIL_HOST
EMAIL_PORT
ALLOWED_HOSTS
- comma-separated list of hosts
RECIPIENT_LIST
- comma-separated list of email recipients
```
